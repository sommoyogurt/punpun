#ifndef ENGINES_CONNECTION_
#define ENGINES_CONNECTION_


#include <memory>
#include <boost/asio/ip/tcp.hpp>
#include "helpers/request.hpp"
#include "helpers/reply.hpp"
#include "helpers/request_handler.hpp"
#include "helpers/request_parser.hpp"
#include <glog/logging.h>
#include <boost/asio/write.hpp>

using ba = boost::asio::ip::tcp;

namespace punpun{
    class connection: public std::enable_shared_from_this<connection>{
    public:
        typedef std::shared_ptr<connection> connection_ptr;
        connection(const connection&) = delete;
        connection& operator=(const connection&) = delete;
        explicit connection(ba::socket socket,request_handler&,std::function<void(connection_ptr)>&);
        void start();
        void stop();
    private:
        ba::socket socket_;
        request_handler& request_handler_;
        std::array<char, 8192> buffer_;
        request request_;
        request_parser request_parser_;
        reply reply_;
        std::function<void(connection_ptr)> func;
        void read_();
        void write_();

    };
}

#endif