#include "timer.hpp"


timer::timer(asio::io_service &io): timer_(io,boost::posix_time::seconds(1)), count_(0) {
    timer_.async_wait(boost::bind(&timer::time,this));
}

timer::~timer(){
    VLOG(1) << count_;
};

void timer::time(){
    if(count_<5){
        VLOG(1) << count_;
        ++count_;
            timer_.expires_at(timer_.expires_at() + boost::posix_time::seconds(1));
            timer_.async_wait(boost::bind(&timer::time,this));
    }
}
