#include "connection.hpp"




void punpun::connection::read_() {
    auto self(shared_from_this());
    socket_.async_read_some(boost::asio::buffer(buffer_), [this,self](boost::system::error_code ec, std::size_t bt){
        if(!ec){
            request_parser::result_type result;
            std::tie(result,std::ignore) = request_parser_.parse(request_, buffer_.data(), buffer_.data() + bt);
            DLOG(INFO)<<result<<" - "<<request_parser::good;
            if(result==request_parser::good){
                DLOG(INFO)<<request_.method;
                request_handler_.handle_request(request_,reply_);
                DLOG(INFO)<<reply_.status;
                write_();
            }
        }
        if(ec!=boost::asio::error::operation_aborted) {
            func(shared_from_this());
        }
    });
}

void punpun::connection::write_() {
    auto self(shared_from_this());
    boost::asio::async_write(socket_, reply_.to_buffers(), [this,self](boost::system::error_code ec, std::size_t bt){
        if(!ec){
            boost::system::error_code ignored_ec;
            socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both,ignored_ec);
        }
        if(ec!=boost::asio::error::operation_aborted) {
            func(shared_from_this());
        }
    });
}

void punpun::connection::start() {
    read_();
}

void punpun::connection::stop() {
    socket_.close();
}


punpun::connection::connection(ba::socket socket, request_handler& handler,std::function<void(connection::connection_ptr)>& func):
        socket_(std::move(socket)),
        request_handler_(handler),
        func(func)
{


}
