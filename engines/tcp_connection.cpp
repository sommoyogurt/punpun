#include "tcp_connection.hpp"

boost::shared_ptr <punpun::tcp_connection> punpun::tcp_connection::create(boost::asio::io_service & io) {
    return pointer(new tcp_connection(io));
}

punpun::tcp_connection::tcp_connection(boost::asio::io_service &io): socket_(io) {

}

boost::asio::ip::tcp::socket& punpun::tcp_connection::socket() {
    return socket_;
}

void punpun::tcp_connection::start() {
    message_ = "msg";
    boost::asio::async_write(socket_, boost::asio::buffer(message_),
            boost::bind(&tcp_connection::handle_write,
                    shared_from_this(),
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));

}

void punpun::tcp_connection::handle_write(const boost::system::error_code& /*error*/,size_t /*bytes_transfered*/ ) {

}
