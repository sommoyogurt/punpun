#ifndef ENGINES_TCP_SERVER_
#define ENGINES_TCP_SERVER_

#include "connection_manager.hpp"
#include "connection.hpp"
#include <boost/asio.hpp>
#include <glog/logging.h>
#include <signal.h>
#include <helpers/routers.hpp>


using namespace boost::asio::ip;

namespace punpun {
    class tcp_server {
    public:
        tcp_server(const std::string &address, int port,const std::string &doc_root,routers &rt);

        ~tcp_server();

        void start();

        void stop();

    private:
        std::string address_;
        int port_;
        boost::asio::io_service io_;
        tcp::acceptor acceptor_;
        tcp::socket socket_;
        boost::asio::signal_set signals_;
        connection_manager connection_manager_;
        request_handler request_handler_;
        void init_();
        void init_acceptor_();
        void accept_();
        void add_signals_();
        void await_stop();
    };
}
#endif