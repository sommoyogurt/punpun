#ifndef ENGINES_TCP_CONNECTION_
#define ENGINES_TCP_CONNECTION_

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>


namespace punpun {
    class tcp_connection: public boost::enable_shared_from_this<tcp_connection> {
    public:
        typedef boost::shared_ptr<tcp_connection> pointer;
        static pointer create(boost::asio::io_service &io);
        boost::asio::ip::tcp::socket &socket() ;
        void start();
    private:
        std::string message_;
        boost::asio::ip::tcp::socket socket_;
        tcp_connection(boost::asio::io_service &io);
        void handle_write(const boost::system::error_code&,size_t);
    };
}
#endif