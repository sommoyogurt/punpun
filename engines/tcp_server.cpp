#include "tcp_server.hpp"

punpun::tcp_server::tcp_server(const std::string &address, int port, const std::string &doc_root, routers &rt) : address_(address),
                                                              port_(port),
                                                              socket_(io_),
                                                              signals_(io_),
                                                              acceptor_(io_),
                                                              request_handler_(doc_root,rt)
{
    init_();
}

punpun::tcp_server::~tcp_server() {
    stop();
}

void punpun::tcp_server::init_() {
    add_signals_();
    await_stop();
    init_acceptor_();
    accept_();
}

void punpun::tcp_server::start() {
    io_.run();
}

void punpun::tcp_server::stop() {
}

void punpun::tcp_server::accept_() {
    acceptor_.async_accept(socket_,[this](boost::system::error_code ec)
    {
        if(!acceptor_.is_open()){
            return;
        }
        if(!ec){
            VLOG(1) << ec;
            connection_manager_.start(std::make_shared<connection>(std::move(socket_),request_handler_,connection_manager_.abort_func));
        }
        accept_();
    });
}

void punpun::tcp_server::add_signals_() {
    signals_.add(SIGINT);
    signals_.add(SIGTERM);
    #if defined(SIGQUIT)
        signals_.add(SIGQUIT);
    #endif
}

void punpun::tcp_server::await_stop() {
    signals_.async_wait([this](boost::system::error_code /*ec*/,int /*signo*/){
        acceptor_.close();
    });

}

void punpun::tcp_server::init_acceptor_() {
    tcp::endpoint endpoint(address::from_string(address_.c_str()),port_);
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();
}
