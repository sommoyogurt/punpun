#include "connection_manager.hpp"

punpun::connection_manager::connection_manager() {

}

void punpun::connection_manager::start(connection::connection_ptr ptr) {
    connections_.insert(ptr);
    ptr->start();
}

void punpun::connection_manager::stop(connection::connection_ptr ptr) {
    connections_.erase(ptr);
    ptr->stop();
}

void punpun::connection_manager::stop_all() {
    for(auto c : connections_){
        c->stop();
    }
    connections_.clear();
}
