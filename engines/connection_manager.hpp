#ifndef CONNECTION_MANAGER_
#define CONNECTION_MANAGER_

#include <set>
#include <boost/smart_ptr/shared_ptr.hpp>
#include "connection.hpp"
#include <iostream>

namespace punpun{
    class connection_manager{
    public:
        connection_manager(const connection_manager&) = delete;
        connection_manager& operator=(const connection_manager&) = delete;
        connection_manager();
        void start(connection::connection_ptr);
        void stop(connection::connection_ptr);
        void stop_all();
        std::function<void(connection::connection_ptr)> abort_func = [this](connection::connection_ptr ptr)->void {
            stop(ptr);
        };
    private:
        std::set<connection::connection_ptr> connections_;


    };
}

#endif