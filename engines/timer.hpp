#ifndef ENGINES_TIMER_
#define ENGINES_TIMER_


#include <iostream>
#include <asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <glog/logging.h>


class timer{
public:
    timer(asio::io_service &io);
    ~timer();
    void time();
private:
    asio::deadline_timer timer_;
    int count_;
};
#endif