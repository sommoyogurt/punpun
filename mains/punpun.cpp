#include "helpers/config.hpp"
#include "helpers/define.hpp"
#include "helpers/routers.hpp"
#include "routers/test.hpp"
//#include "engines/timer.hpp"
#include "engines/tcp_server.hpp"
#include <glog/logging.h>

void handler_cb(){
}



class route_base {
public:
    route_base(){};
private:
    std::string path;
};

class route_test: public route_base{

};




int _punpun(int *argc,char* argv[]){
    int port;
    std::string host;

    routers rt;
    rt.add_route(std::shared_ptr<router_base>(new router::router_test("/test",std::vector<punpun::REQMETHOD>{punpun::REQMETHOD::GET})));


    punpun::helpers::config config(DEFAULT_CONF_FILE);
    config.lookup("server.tcp.port",port);
    config.lookup("server.tcp.host",host);
    LOG(INFO) << "Bind Tcp: "<<host<<":"<<port;
    punpun::tcp_server server(host,port,"/",rt);
    server.start();
    //asio::io_service io;
    //timer t(io);
    //io.run();
   /* asio::ip::tcp::endpoint end_point(asio::ip::address::from_string(host),port);
    asio::io_service io_service;
    asio::ip::tcp::socket socket(io_service);
    */

    return 0;
}