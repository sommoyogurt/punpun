#include "config.hpp"


punpun::helpers::config::config(std::string file_path): file_path_(file_path){
    read_();
}

punpun::helpers::config::~config(){
}

void punpun::helpers::config::read_(){
    file_exist_ = false;
    ifs_ = fopen(file_path_.c_str(), "r");
    if(ifs_!=NULL) {
        cfg_.read(ifs_);
        fclose(ifs_);
        file_exist_ = true;
    }else{
        LOG(ERROR)<<"No file config found in "<<file_path_;
    }
}

bool punpun::helpers::config::lookup(const std::string &path, std::string &val) const {
    return cfg_.lookupValue(path.c_str(), val);
}

bool punpun::helpers::config::lookup(const std::string &path, int &val) const {
    return cfg_.lookupValue(path.c_str(), val);
}
