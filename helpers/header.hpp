#ifndef HELPERS_HEADER_
#define HELPERS_HEADER_

#include <string>

namespace punpun {
    struct header {
        std::string name;
        std::string value;
    };
}

#endif
