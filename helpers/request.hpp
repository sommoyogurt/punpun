#ifndef HELPERS_REQUEST_
#define HELPERS_REQUEST_

#include "header.hpp"
#include <string>
#include <vector>

namespace punpun {

    enum class REQMETHOD {
        GET,
        POST,
        PUT,
        UPDATE,
        DELETE,
        CREATE
    };
    struct request {
        std::string method;
        std::string uri;
        int http_version_major;
        int http_version_minor;
        std::vector <header> headers;
    };
}

#endif
