#ifndef HELPERS_ROUTERS_
#define HELPERS_ROUTERS_

#include <vector>
#include <map>
#include <string>
#include "routers/router_base.hpp"
#include "request.hpp"
#include "reply.hpp"
using namespace punpun;

class routers{
public:
    routers();
    ~routers();
    void add_route(std::shared_ptr<router_base> rb);
    void response(const request &req, reply &rep);
private:
    typedef std::map<const std::string,std::shared_ptr<router_base>> t_uri_map;

    t_uri_map uri_map_;
    std::map<punpun::REQMETHOD,t_uri_map> router_map_;
    std::map<std::string,punpun::REQMETHOD> method_map_;
};

#endif