#include <glog/logging.h>
#include "routers.hpp"

routers::routers():method_map_{{"GET",punpun::REQMETHOD::GET}} {
}
routers::~routers() {
}
void routers::add_route(std::shared_ptr<router_base> rb){
   // rt.push_back(rb);
    auto vm = rb->valid_method();
    std::for_each(vm->begin(), vm->end(),[&](punpun::REQMETHOD v){
        t_uri_map um{{rb->uri(),rb}};
        router_map_.insert(std::pair<punpun::REQMETHOD, t_uri_map>(v,um));
    });
}

void routers::response(const request &req, reply &rep) {
    DLOG(INFO) << req.uri<<" ["<<req.method<<"]";
    auto res = router_map_.at(method_map_.at(req.method)).at(req.uri);
    res->response(req,rep);
}
