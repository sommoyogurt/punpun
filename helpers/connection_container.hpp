#ifndef HELPERS_CONNACTION_CONTAINER_
#define HELPERS_CONNACTION_CONTAINER_

#include "engines/connection.hpp"

struct connection_container{
    std::set<connection::connection_ptr> connections;
};

#endif