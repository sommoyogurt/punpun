#ifndef HELPERS_CONFIG_
#define HELPERS_CONFIG_

#include <libconfig.h++>
#include <string>
#include <stdio.h>
#include <glog/logging.h>


using namespace libconfig;

namespace punpun{
    namespace helpers{
        class config {
        public:
            config(std::string file_path);
            ~config();
            bool lookup(const std::string &path,int &val) const;
            bool lookup(const std::string &path,std::string &val) const;
        private:
            bool file_exist_;
            std::string file_path_;
            FILE *ifs_;
            Config cfg_;
            void read_();
        };
    }
}
#endif