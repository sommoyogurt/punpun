#include <glog/logging.h>
#include "request_handler.hpp"

punpun::request_handler::request_handler(const std::string &doc_root,routers &rt)
        : doc_root_(doc_root), routers_(rt)
{

}

void punpun::request_handler::handle_request(punpun::request const &req, punpun::reply &rep) {
    std::string request_path;
    if(!url_decode(req.uri, request_path)){
        DLOG(ERROR)<<"Error url decode";
        rep = reply::stock_reply(reply::bad_request);
    }
    if(request_path.empty() || request_path[0]!='/' || request_path.find("..")!= std::string::npos){
        rep = reply::stock_reply(reply::bad_request);
        DLOG(ERROR)<<"Error url path";
    }
    if(request_path[request_path.size()-1] == '/'){
        request_path += "index.html";
    }
    routers_.response(req,rep);
    rep.status = reply::ok;
    std::string extension = "json";
    //rep.content= "{\"project\":\"rapidjson\",\"stars\":10}";
    rep.headers.resize(2);
    rep.headers[0].name = "Content-Length";
    rep.headers[0].value = std::to_string(rep.content.size());
    rep.headers[1].name = "Content-Type";
    rep.headers[1].value = mime_types::extension_to_type(extension);

}

bool punpun::request_handler::url_decode(const std::string &in, std::string &out) {
    out.clear();
    out.reserve(in.size());
    DLOG(INFO)<<"In string "<<in<<" "<<in.size();
    for(std::size_t i = 0;i<in.size();++i) {
        if (in[i] == '%') {
            if (i + 3 <= in.size()) {
                int value = 0;
                std::istringstream is(in.substr(i + 1, 2));
                if (is >> std::hex >> value) {
                    out += static_cast<char>(value);
                    i += 2;
                } else {
                    DLOG(ERROR)<<"Error string stream ";
                    return false;
                }
            } else {
                DLOG(ERROR)<<"Error url size";
                return false;
            }
        } else if (in[i] == '+') {
            out += ' ';
        } else {
            out += in[i];
        }
    }
    return true;
}
