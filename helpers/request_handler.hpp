#ifndef HELPERS_REQUEST_HANDLER_
#define HELPERS_REQUEST_HANDLER_

#include <iostream>
#include "request.hpp"
#include "reply.hpp"
#include "mime_types.hpp"
#include "routers.hpp"
#include <sstream>

namespace punpun {
    class request_handler {
    public:
        request_handler(const request_handler&) = delete;
        request_handler& operator=(const request_handler&) = delete;
        explicit request_handler(const std::string& doc_root,routers &rt);
        void handle_request(const request& req, reply& rep);
    private:
        std::string doc_root_;
        routers routers_;
        static bool url_decode(const std::string& in,std::string& out);
    };
}

#endif
