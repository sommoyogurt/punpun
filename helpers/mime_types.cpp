#include "mime_types.hpp"

namespace punpun{
    namespace mime_types{

        struct  mapping {
            const char *extension;
            const char *mime_type;
        } mappings[] = {{"json", "application/json"}, {"gif", "image/gif"},
                {"htm", "text/html"},
                {"html", "text/html"},
                {"jpg", "image/jpeg"},
                {"png", "image/png"},
                {"txt", "text/plain"}};

        std::string default_mime_type = "text/plain";

        const std::string extension_to_type(const std::string &ext) {
            for(mapping m: mappings){
                if(m.extension == ext){
                    return m.mime_type;
                }
            }
            return default_mime_type;
        }
    }
}



