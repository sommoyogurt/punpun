#ifndef HELPERS_MIME_TYPES_
#define HELPERS_MIME_TYPES_

#include <string>
namespace punpun {
    namespace mime_types {


            const std::string extension_to_type(const std::string &ext);

    }
}

#endif