#include <glog/logging.h>
#include "router_base.hpp"

router_base::router_base(const std::string  &uri, std::vector<punpun::REQMETHOD> vm):uri_(uri),vm_(vm){

}
router_base::~router_base(){

}

const std::string router_base::uri() const {
    return uri_;
}
std::vector<punpun::REQMETHOD>* router_base::valid_method(){
    return &vm_;
}

void router_base::response(const punpun::request &req, punpun::reply &rep) {
    DLOG(INFO) << "default response";
    rep.content= "{\"project\":\"rapidjson\",\"stars\":10,\"uri\":\""+uri_+"\"}";
}
