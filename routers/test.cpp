#include <glog/logging.h>
#include "test.hpp"


router::router_test::router_test(const std::string &uri,std::vector<punpun::REQMETHOD> vm): router_base(uri,vm) {
}
router::router_test::~router_test() {
}
void router::router_test::response(const punpun::request &req, punpun::reply &rep) {
    DLOG(INFO) << "test response";
    rep.content= "{\"project\":\"rapidjson\",\"stars\":10,\"uri\":\""+uri_+"\"}";
}