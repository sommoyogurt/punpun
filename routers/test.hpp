#ifndef ROUTERS_TEST_
#define ROUTERS_TEST_

#include "router_base.hpp"
#include <string>
#include "helpers/request.hpp"
namespace router {

    class router_test : public router_base {
    public:
        router_test(const std::string &uri,std::vector<punpun::REQMETHOD> vm);
        virtual ~router_test();
        void response(const punpun::request &req,punpun::reply &rep);

    };
}

#endif