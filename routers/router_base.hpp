#ifndef ROUTERS_ROUTER_BASE_
#define ROUTERS_ROUTER_BASE_

#include <string>
#include <helpers/reply.hpp>
#include "helpers/request.hpp"

class router_base{
public:
    router_base(const std::string  &uri, std::vector<punpun::REQMETHOD> vm);
    router_base(const router_base&) = delete;
    router_base& operator=(const router_base&) = delete;
    const std::string uri() const;
    std::vector<punpun::REQMETHOD>* valid_method();
    virtual void response(const punpun::request &req,punpun::reply &rep);
    virtual ~router_base();

protected:
    const std::string uri_;
    std::vector<punpun::REQMETHOD> vm_;
};

#endif