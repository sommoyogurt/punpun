#include <glog/logging.h>
#include "helpers/gflags_define.hpp"
#include "helpers/define.hpp"
#include "mains/test.cpp"
#include "mains/punpun.cpp"


int main(int argc,char* argv[]){
    google::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir=DEFAULT_LOG_DIR;
    FLAGS_stderrthreshold=0;
    FLAGS_v=1;

    if(FLAGS_test){
        return _test(&argc, argv);
    }else{
        return _punpun(&argc,argv);
    }
}